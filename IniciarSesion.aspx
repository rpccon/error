﻿<%@ Page Title="Iniciar Sesión" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="IniciarSesion.aspx.cs" Inherits="SICRER_Web_Application.IniciarSesion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    
    <div class="alert alert-danger" id="notificacion" style="display: none;" role="alert">
        <a class="close" data-dismiss="alert">×</a>
        <h5 class="alert-heading">Warning!</h5>
        <p id="msg" runat="server" style="font-size: 12px;"></p>
    </div>


    <div class="container" style="margin-top: 40px">

        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Inicie sesión para continuar</strong>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="row">
                                <div class="center-block">
                                    <img class="profile-img" src="img/lock.png" alt="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                            <asp:TextBox ID="TextBox1" CssClass="form-control" placeholder="Usuario" runat="server" ></asp:TextBox>
<%--                                            <asp:RequiredFieldValidator Enabled="true" ID="RequiredFieldValidator2"
                                                runat="server"
                                                ErrorMessage="Digite usuario valido" ControlToValidate="TextBox1"
                                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />--%>
                                           
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-lock"></i>
                                            </span>
                                            <asp:TextBox ID="TextBox2" placeholder="Contrasena" TextMode="Password" CssClass="form-control" runat="server"></asp:TextBox>
           <%--                                 <asp:RequiredFieldValidator Enabled="true" ID="RequiredFieldValidator3"
                                                runat="server"
                                                ErrorMessage="Digite contraseña valida"  ControlToValidate="TextBox2"
                                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                           --%>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <!--<input type="submit" class="btn btn-lg btn-primary btn-block" value="Iniciar Sesión"/>-->
                                        <asp:Button ID="Button1" CssClass="btn btn-lg btn-primary btn-block" OnClick="btnIniciarSesion" runat="server" Text="Iniciar Sesion" />
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="panel-footer ">
                        ¿Ha olvidado su contraseña?
                        <asp:LinkButton ID="LinkButton1" data-toggle="modal"  data-target="#olvidoContrasena" runat="server">Continuar</asp:LinkButton>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade validator" id="olvidoContrasena" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Recuperación de Datos</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Aviso!</strong> El restablecimiento de la contraseña solo es posible para usuarios Administradores,
                        si el usuario es un Revisor o Coordinador deberá acudir a los Administradores para solicitar la contraseña. 
                    </div>

                    <div class="form-group">
                        <label for="inputEmail1" class="control-label">Email</label>
                        <input type="email" class="form-control" runat="server" id="InputEmail1" placeholder="Email"/>
                    </div>
                  <%--  <asp:RequiredFieldValidator Enabled="true" ID="RequiredFieldValidator1"
                        runat="server"
                        ErrorMessage="Digite correo valido" ControlToValidate="InputEmail1"
                        Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />--%>
                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default"  data-dismiss="modal">Cerrar</button>
                    <asp:LinkButton ID="LinkButton2" OnClick="LinkButton2_Click"  CssClass="btn btn-primary" runat="server">Enviar</asp:LinkButton>
                </div>
            </div>
        </div>

    </div>


</asp:Content>
