﻿<%@ Page Title="Registrar Residencias" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registro_Residencias.aspx.cs" Inherits="SICRER_Web_Application.Registro_Residencias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/personalizado.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-1.11.4.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-theme-1.11.4.css" rel="stylesheet" />
    <link href="Content/revisiones.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.11.4.js"></script>
    <script src="Scripts/jquery.ui.datepicker-es.js"></script>
    <script src="Scripts/revisiones.js"></script>
    <script src="Scripts/site.js"></script>    
    <script>
        function inhabilitarRequest() {
            document.getElementById('<%=RequiredFieldValidatorResidencia.ClientID %>').enabled = false;
        }
    </script>
    <div class="col-md-12">
        <div class="col-md-3 space-up">
            <ul class="nav nav-pills nav-stacked well">
                <li><a href="Registrar_Estudiantes.aspx">Estudiantes</a></li>
                <li  class="active"><a href="Registro_Residencias.aspx">Residencias</a></li>
            </ul>
        </div>

         <div class="col-md-6 space-up" style="align-content: center">

            <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Residencias</div>
                </div>
                <div class="panel-body form-horizontal">
                    <asp:ValidationSummary runat="server" ID="ValidationSummary1"
                        DisplayMode="BulletList"
                        ShowMessageBox="False" ShowSummary="True" CssClass="alert alert-danger" />
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Numero de Residencia
                        </label>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" CssClass="form-control" ControlToValidate="tbnumResidencia" EnableClientScript="true" TextMode="Number" ID="tbnumResidencia"  />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorResidencia"
                                runat="server"
                                ErrorMessage="Digite Numero Residencia" ControlToValidate="tbnumResidencia"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8" style="align-content:center">
                            <asp:LinkButton runat="server" Text="<span class='glyphicon glyphicon-book'> </span> Registrar " CssClass="btn btn-primary" OnClick="Unnamed1_Click" />
                            <asp:LinkButton runat="server" Text="<span class='glyphicon glyphicon-edit'> </span> Actualizar " CssClass="btn btn-warning" OnClick="Unnamed2_Click" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
          <div class="col-md-3 space-up">
              <asp:TreeView ID="TreeView1" runat="server" ImageSet="WindowsHelp">
                  <HoverNodeStyle Font-Underline="True" ForeColor="#6666AA" />
                  <NodeStyle Font-Names="Tahoma" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="1px" />
                  <ParentNodeStyle Font-Bold="False" />
                  <SelectedNodeStyle BackColor="#B5B5B5" Font-Underline="False" HorizontalPadding="0px" VerticalPadding="0px" />
              </asp:TreeView>
        </div>
    </div>
    <script src="Scripts/bootstrap.min.js"></script>
</asp:Content>
