﻿<%@ Page Title="Revisión" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Revisiones.aspx.cs" Inherits="SICRER_Web_Application.Revisiones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/jquery-ui-smoothness-1.11.4.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-theme-1.11.4.css" rel="stylesheet" />
    <link href="Content/revisiones.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.11.4.js"></script>
    <script src="Scripts/jquery.ui.datepicker-es.js"></script>
    <script src="Scripts/revisiones.js"></script>
    <script src="Scripts/site.js"></script>
    <script>
        function advice() {

            alert('hola');
        }
    </script>
    <style>
        @media only screen and (max-width: 768px) {
             .form-inline {
                display: block;
                margin-top: 10px;
                margin-bottom:10px;
            }
            .btn-primary btn-default{
                margin-top: 10px;
                margin-bottom:5px;
            }
            .form-control{
                margin-bottom:5px;
            }
        }
    </style>
    <div class="col-md-12">
        <div class="page-header">
                <asp:Label  font-size="32px" ID="title" runat="server">Generar Revisión</asp:Label>
              
        </div>
    </div>

    <div class="col-md-9 space-up">
        <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
            <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
            <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
            <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
            <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <div class="form-inline" >
            <asp:DropDownList id="ddTp_ind" runat="server" CssClass="selectpicker form-control" Width="130px" OnSelectedIndexChanged="ddTp_ind_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Text="Cocina" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Cuarto" Value="2"></asp:ListItem>
            </asp:DropDownList>
             <asp:DropDownList id="PDF_tipos" runat="server" CssClass="selectpicker form-control" Width="130px">
                <asp:ListItem Text="Cocina" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Cuarto" Value="2"></asp:ListItem>
            </asp:DropDownList>
             <asp:DropDownList Visible="false" id="rooms" runat="server" CssClass="selectpicker form-control" Width="130px">
                <asp:ListItem Text="Cuarto" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="1" Value="2"></asp:ListItem>
                 <asp:ListItem Text="2" Value="3"></asp:ListItem>
                 <asp:ListItem Text="3" Value="4"></asp:ListItem>
                 <asp:ListItem Text="4" Value="5"></asp:ListItem>
                 <asp:ListItem Text="5" Value="6"></asp:ListItem>
                 <asp:ListItem Text="6" Value="7"></asp:ListItem>
                 <asp:ListItem Text="7" Value="8"></asp:ListItem>
                 <asp:ListItem Text="8" Value="9"></asp:ListItem>
                 <asp:ListItem Text="9" Value="10"></asp:ListItem>
                 <asp:ListItem Text="10" Value="11"></asp:ListItem>
                 <asp:ListItem Text="11" Value="12"></asp:ListItem>
                 <asp:ListItem Text="12" Value="13"></asp:ListItem>
                 <asp:ListItem Text="13" Value="14"></asp:ListItem>
                 <asp:ListItem Text="14" Value="15"></asp:ListItem>
                 <asp:ListItem Text="15" Value="16"></asp:ListItem>
                 <asp:ListItem Text="16" Value="17"></asp:ListItem>

            </asp:DropDownList>
            <asp:DropDownList ID="ddResidencias" runat="server" DataTextField="Residencia" CssClass="selectpicker form-control" Width="130px">
            </asp:DropDownList>
            
            <asp:DropDownList ID="ddAlas" runat="server" CssClass="selectpicker form-control" Width="130px">
                <asp:ListItem Text="Ala" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="A" Value="2"></asp:ListItem>
                <asp:ListItem Text="B" Value="3"></asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="PDFAlas" runat="server" CssClass="selectpicker form-control" Width="130px">
                <asp:ListItem Text="Ala" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="A" Value="2"></asp:ListItem>
                <asp:ListItem Text="B" Value="3"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox id="PDFA" runat="server" ReadOnly="true"  CssClass="form-control" Width="130px"/>
            <asp:TextBox id="PDFB" runat="server" ReadOnly="true"  CssClass="form-control" Width="130px"/>
            <asp:DropDownList ID="NP_alas" visible="false" runat="server" CssClass="selectpicker form-control" Width="130px">
                <asp:ListItem Text="Ala" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="A" Value="2"></asp:ListItem>
                <asp:ListItem Text="B" Value="3"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox id="NP_inicial" Visible="false" runat="server"  CssClass="form-control" Width="130px"/>
            <asp:TextBox id="NP_final" Visible="false" runat="server"  CssClass="form-control" Width="130px"/>
          
            <asp:LinkButton id="linkBuscar" runat="server" Text="<span class='glyphicon glyphicon-search'> </span> Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click"/>
            <asp:LinkButton id="LinkButton1" runat="server" Text="<span class='glyphicon glyphicon-user'></span> Generar PDF"  CssClass="btn btn-danger" OnClick="openModalPDF"></asp:LinkButton>
         

            <asp:LinkButton id="linkEstudiantesRev" runat="server" Text="<span class='glyphicon glyphicon-user'></span> Estudiantes" Visible="false" CssClass="btn btn-default pull-right" data-toggle="modal" data-target="#modalEst"></asp:LinkButton>
            
            
            
        </div>

                <!-- Modal para generar PDF -->
        <!--<div id="modal_PDF" class="modal fade" role="dialog">
            <div class="modal-dialog  ">

    
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 runat="server" align="center" class="modal-title">Seleccione el ala</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                       <!-- <asp:Panel ID="panelPDF_info_Exito" runat="server" CssClass="exito" Visible="false">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/ok.png"/>
                            <asp:Label ID="Label1" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="panelPDF_info_Error" runat="server" CssClass="alerta" Visible="false">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/img/warning_icon.png"/>
                            <asp:Label ID="Label2" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </asp:Panel>-->
                        <!--<asp:DropDownList id="DropDownList1" runat="server" CssClass="selectpicker form-control" Width="130px">
                            <asp:ListItem Text="Cocina" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Cuarto" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DropDownList2" runat="server" DataTextField="Residencia" CssClass="selectpicker form-control" Width="130px">
                        </asp:DropDownList>
                        <div align="center">
                         <asp:DropDownList ID="DropDownList3" runat="server" DataTextField="Ala" CssClass="selectpicker form-control" Width="130px">
                                <asp:ListItem Text="Ala" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="A" Value="2"></asp:ListItem>
                                <asp:ListItem Text="B" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                            <asp:TextBox id="PDF_FechaInicio" runat="server" TextChanged="openCalendar_click" CssClass="form-control" Width="130px"/>
                     
                            <asp:TextBox id="PDF_FechaFinal" runat="server" CssClass="form-control" Width="130px"/>
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:ImageButton ID="ImageButton1" Height="23px" Width="36px" ImageUrl="~/img/tec.jpg" runat="server" />

                        </div>
                        <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
                        <asp:Calendar ID="Calendar2" runat="server"></asp:Calendar>

                      

                    </div>

                </div>
                <div class="modal-footer">


                    <button type="button" class="btn btn-default" >Cerrar</button>
                </div>
            </div>
            </div>
            </div>-->

        <!-- Modal Estudiante Encontrados -->
        <div id="modalEst" class="modal fade" role="dialog">
            <div class="modal-dialog  ">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 runat="server" class="modal-title">Estudiantes de la Revisión</h4>
                </div>
                <div class="modal-body">
                    <p id="contenidoEstRev" runat="server"></p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            </div>
            </div>
        <div style="margin-left: 300px; margin-right: auto; text-align: center;">
            <asp:Label  Width="300" ID="lb_ask_kinds" style="text-align:center"  Visible="false" runat="server" Font-Size="20" Font-Bold="True">¿ Qué desea generar ?</asp:Label>     
        
        </div>
       <!-- <div style="margin-left: 500px; margin-right: auto; text-align: center; padding:10px;">
             <asp:DropDownList ID="listaTipo" Visible="false" runat="server" CssClass="selectpicker form-control" Width="130px">
                    <asp:ListItem Text="Tipo" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Revisiones general" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Revisiones con llamadas de atención" Value="3"></asp:ListItem>
            </asp:DropDownList>       

            <div  class="pull-left space-up space-down-footer">
                <asp:LinkButton Visible="false" ID="continuar_selected" runat="server" Text="<span class='glyphicon glyphicon-book'> </span> Continuar " CssClass="btn btn-success" OnClick="btnGuardar_Click"/>
            </div>
        </div>-->
        <div id="panel_CCC" style="margin-left: 310px;" class="panel panel-default">
                 <div id="div_title" style="text-align:center;" class="panel-heading">
                        <div class="panel-title"><h3>Indique el tipo de PDF a generar</h3></div>
                </div>
                <div id="panel_setDewewfsalojo" align="center"  class="panel-body form-horizontal">
                    <div id="subpanel_B" class="form-inline space-up space-down">
             <asp:DropDownList ID="selected_ops"  runat="server" CssClass="selectpicker form-control" Width="130px">
                    <asp:ListItem Text="Revisiones general" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Revisiones con llamadas de atención" Value="2"></asp:ListItem>
            </asp:DropDownList> 
                    <asp:LinkButton ID="kind_selected" runat="server"  AutoPostBack="true"   Text="<span class='glyphicon glyphicon-book'> </span> Continuar "  CssClass="btn btn-success" OnClick="analycePDFselection"/>

                       </div>
		           </div>


       </div>


        
        <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="table-responsive">
                    
                    <asp:GridView id="gvInd" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover space-up">
                        <Columns>
                            <asp:BoundField  DataField="Indicador" HeaderText="Rubros de Calificación"/>

                            <asp:TemplateField HeaderText="Excelente">
                                <ItemTemplate>
                                    <asp:RadioButton ID="rbMBueno" runat="server" GroupName="groupCalif" Checked="true"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Bueno">
                                <ItemTemplate>
                                    <asp:RadioButton ID="rbBueno" runat="server" GroupName="groupCalif" Checked="false" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Regular">
                                <ItemTemplate>
                                    <asp:RadioButton ID="rbRegular" runat="server" GroupName="groupCalif" Checked="false"/>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Deficiente">
                                <ItemTemplate>
                                    <asp:RadioButton ID="rbDeficiente" runat="server" GroupName="groupCalif" Checked="false"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:Button ID="btnCalificar" runat="server" CssClass="btn btn-warning" Text="Obtener Calificación" OnClick="btnCalificar_Click" OnClientClick="loading('MainContent_imgCargando');"/>
                <asp:Image ID='imgCargando' runat='server' ImageUrl='~/img/load_transaction.gif' CssClass="hide-ele"/>
                <div id="porcentaje" runat="server" class="space-down space-up porcentaje" visible="false">0%</div>

                <asp:Panel ID="panelMsjErrorCalif" runat="server" CssClass="space-down space-up error" Visible="false">
                    <asp:Image ID="imgIndErrorCalif" runat="server" ImageUrl="~/img/error_icon.png"/>
                    <asp:Label ID="lbIndErrorCalif" runat="server" Text="" Font-Bold="true"></asp:Label>
                </asp:Panel>

            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="space-up">
            <asp:Label ID="text" runat="server">Observaciones</asp:Label>
          
            <asp:TextBox ID="textAreaObserv" runat="server" Height="150px" Width="400px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="pull-left space-up space-down-footer">
            <asp:LinkButton ID="linkGuardar" runat="server" Text="<span class='glyphicon glyphicon-book'> </span> Guardar " CssClass="btn btn-success" OnClick="btnGuardar_Click"/>
        </div>
        <script src="Scripts/bootstrap.min.js"></script>
    </div>
</asp:Content>
