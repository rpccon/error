﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LlamadasAtencion.aspx.cs"  MasterPageFile="~/Site.Master" Inherits="SICRER_Web_Application.Graficos.LlamadasAtencion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <link href="Content/jquery-ui-smoothness-1.11.4.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-theme-1.11.4.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Styles -->
<style>
#chartdiv {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}							
</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />


<!-- Chart code -->
        <style>
        @media only screen and (max-width: 768px) {
             .form-inline {
                display: block;
                margin-top: 10px;
                margin-bottom:10px;
            }
            .rbEspacio input[type="radio"]
            {
               margin-left: 40px;
               margin-right: 1px;
            }
            .paraDiv
            {
                border-left-style: solid;
            }
            .btn-primary btn-default{
                margin-top: 10px;
                margin-bottom:5px;
            }
            .form-control{
                margin-bottom:5px;
            }
        }
    </style>



        <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li><a href="CumpIndicadores.aspx">Cumplimiento de indicadores</a></li>
            <li><a href="IncumpIndicadores.aspx.aspx">Incumplimiento de indicadores</a></li>
            <li><a href="AlasCumpResis.aspx">Cumplimiento de residencias por alas</a></li>
            <li><a href="CuartosDB.aspx">Cuartos buenos y deficientes</a></li>
            <li class="active"><a href="LlamadasAtencion.aspx">Registros llamadas de atención</a></li>
        </ul>
    </div>
      
    <div class="col-md-9 space-up" style="align-content:center">
             <asp:Panel ID="p_msjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel  ID="p_msjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>
        <ul class="nav nav-pills nav-stacked well">



                
            <li id="prueba" class="form-inline">

                <asp:DropDownList  runat="server" ID="dd_mesesUno" DataTextField="MES" Width="180px" visible="false" CssClass="selectpicker form-control" OnSelectedIndexChanged="dd_mesesUno_click" DataValueField="MES"  AutoPostBack="true">
                </asp:DropDownList> 
                 <asp:DropDownList  runat="server" ID="dd_mesesDos" DataTextField="MES" Width="180px" visible="false" CssClass="selectpicker form-control" OnSelectedIndexChanged="dd_mesesDos_click" DataValueField="MES"  AutoPostBack="true">
                </asp:DropDownList>     
                 <asp:DropDownList  runat="server" ID="dd_mesesTres" DataTextField="MES" Width="180px" visible="false" CssClass="selectpicker form-control" OnSelectedIndexChanged="dd_mesesTres_click" DataValueField="MES"  AutoPostBack="true">
                </asp:DropDownList>                     
                <asp:DropDownList  runat="server" ID="dd_meses" DataTextField="MES" Width="180px"  CssClass="selectpicker form-control" OnSelectedIndexChanged="dd_meses_click" DataValueField="MES"  AutoPostBack="true">
                </asp:DropDownList>        

                <asp:DropDownList  runat="server" ID="II_periodos" DataTextField="Periodo" Width="180px" OnSelectedIndexChanged="II_pers_click" CssClass="selectpicker form-control" DataValueField="Periodo"  AutoPostBack="true">
                </asp:DropDownList>
               

                <asp:RadioButtonList ID="rest"  ValidationGroup="valGroup1" OnSelectedIndexChanged="tipo_filtro_click" runat="server" CssClass="rbEspacio radio" RepeatDirection="Horizontal" AutoPostBack="true" >
                     <asp:ListItem Value="Semestral" Selected="True"/>
                     <asp:ListItem Value="Mensual"  />
                    <asp:ListItem Value="Trimestral"  />
                </asp:RadioButtonList>

   
                    <ul style="display:none" class="nav nav-pills nav-stacked well" >
                            <asp:RadioButtonList ID="rb_cc"  ValidationGroup="valGroup1" OnSelectedIndexChanged="cambio_click" runat="server" CssClass="rbEspacio radio"  RepeatDirection="Horizontal" AutoPostBack="true">
                                 <asp:ListItem Value="Por persona" Selected="True"/>
                                 <asp:ListItem Value="Por cuarto"  />
                            </asp:RadioButtonList>
                    </ul>
            </li>        
        </ul>



    </div>
    <div id="g2panel" class="col-md-9 space-up"> <!--PAEL PARA VISUALIZAR AGRUPAMIENTO POR CUARTOS-->
                     <!--   <div id="panel_BB" class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Resultados</div>
                    </div>-->
                <div class="panel-body form-horizontal">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" RenderMode="Inline" ChildrenAsTriggers="true">
                        <ContentTemplate>
                        <div class="table-responsive">
                            <asp:GridView ID="gridCuartos" runat="server"   AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-hover space-up"  EnableViewState="true" ClientIDMode="Static">
                                <Columns>

                                    <asp:BoundField DataField="NUMERO_CUARTO" HeaderText="NUMERO_CUARTO" SortExpression="NUMERO_CUARTO" />
                                    <asp:BoundField DataField="NUMERO_RESIDENCIA" HeaderText="NUMERO_RESIDENCIA" SortExpression="NUMERO_RESIDENCIA" />
                                    <asp:BoundField DataField="CANTIDAD" HeaderText="CANTIDAD" SortExpression="CANTIDAD" />

                                </Columns>
                            </asp:GridView>
                        </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
              </div>
     


    <div id="gpanel" class="col-md-5 space-up">  <!--PAEL PARA VISUALIZAR AGRUPAMIENTO POR PERSONA-->
                      <!--  <div id="panel_B" class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Resultados</div>
                    </div>-->
                <div  class="panel-body form-horizontal">
                    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional" RenderMode="Inline" ChildrenAsTriggers="true">
                        <ContentTemplate>
                        <div style="align-content:center" class="table-responsive">
                            <asp:GridView ID="gridPersonas" runat="server"   AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-hover space-up" DataKeyNames="CARNE" EnableViewState="true" ClientIDMode="Static">
                                <Columns>

                                    <asp:BoundField DataField="CARNE" HeaderText="CARNE" ReadOnly="True" SortExpression="CARNE" />
                                    <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE" />
                                    <asp:BoundField DataField="APELLIDO1" HeaderText="APELLIDO1" SortExpression="APELLIDO1" />
                                  

                                    <asp:BoundField DataField="NUMERO_CUARTO" HeaderText="NUMERO_CUARTO" SortExpression="NUMERO_CUARTO" />
                                    <asp:BoundField DataField="NUMERO_RESIDENCIA" HeaderText="NUMERO_RESIDENCIA" SortExpression="NUMERO_RESIDENCIA" />
                                    <asp:BoundField DataField="CANTIDAD" HeaderText="CANTIDAD" SortExpression="CANTIDAD" />

                                </Columns>
                            </asp:GridView>
                        </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
              </div>
     

 



</asp:Content>
